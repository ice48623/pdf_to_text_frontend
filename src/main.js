// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import Vuetify from 'vuetify';
import VueSocketio from 'vue-socket.io';
import '@fortawesome/fontawesome-free/css/all.css';
import 'vuetify/dist/vuetify.min.css';
import VueCookies from 'vue-cookies';
import App from './App';
import router from './router';

Vue.use(Vuetify, {
  iconfont: 'fa',
});

Vue.config.productionTip = false;

Vue.use(VueSocketio, '/');

Vue.use(VueCookies);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
});
