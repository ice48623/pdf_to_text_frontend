import Axios from 'axios';

const URL = 'http://pdf-to-txt.peter-struschka.com:30000/';

const api = Axios.create({
  baseURL: URL,
});

export default api;
