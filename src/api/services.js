import api from '@/api/config';

export function uploadFile(file, room) {
  const path = '/upload';
  const header = {
    'Content-Type': 'multipart/form-data',
  };
  const formData = new FormData();
  formData.append('file', file);
  formData.append('room', room);
  return api.post(path, formData, header)
    .then(({ data }) => data)
    .catch(error => error);
}

export function downloadFile(uuid, room) {
  const path = `/download/${room}/${uuid}`;
  return api({
    url: path,
    method: 'GET',
    responseType: 'blob', // important
  })
    .then(res => res)
    .catch(err => err);
}
